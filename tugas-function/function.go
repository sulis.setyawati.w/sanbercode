package main

import (
	"fmt"
	"strings"
)

// fungsi untuk menghitung luas persegi panjang
func luasPersegiPanjang(panjang int, lebar int) (luas int) {
	luas = panjang * lebar
	return
}

// fungsi untuk mrnghitung keliling persegi panjang
func kelilingPersegiPanjang(panjang int, lebar int) (keliling int) {
	keliling = 2 * (panjang + lebar)
	return
}

// fungsi untuk menghitung volume balok
func volumeBalok(panjang int, lebar int, tinggi int) (volume int) {
	volume = panjang * lebar * tinggi
	return
}

// fungsi introduce : mencetak kalimat perkenalan, jika laki laki sapaannya Pak, jika perempuan sapaannnya Bu
func introduce(nama string, gender string, profesi string, usia string) string {
	var introduction string
	switch gender {
	case "laki-laki":
		introduction = "Pak " + nama + " adalah seorang " + profesi + " yang berusia " + usia + " tahun"
	case "perempuan":
		introduction = "Bu " + nama + " adalah seorang " + profesi + " yang berusia " + usia + " tahun"
	}
	return introduction
}

/* 	var buahFavoritJohn = buahFavorit("John", buah...)

fmt.Println(buahFavoritJohn)
// halo nama saya john dan buah favorit saya adalah "semangka", "jeruk", "melon", "pepaya"
*/

func buahFavorit(nama string, buah ...string) string {
	var infoBuahFavorit string
	infoBuahFavorit = "halo nama saya " + nama + " dan buah favorit saya adalah " + strings.Join(buah, `,`)
	return infoBuahFavorit
}

func main() {
	/* Soal 1
	   Tulislah 3 function dengan nama luas persegi panjang, keliling persegi panjang dan volume balok */
	fmt.Println("======= Soal 1 =======")
	panjang := 12
	lebar := 4
	tinggi := 8

	luas := luasPersegiPanjang(panjang, lebar)
	keliling := kelilingPersegiPanjang(panjang, lebar)
	volume := volumeBalok(panjang, lebar, tinggi)

	fmt.Println("Luas persegi panjang =", luas)
	fmt.Println("Keliling Persegi panjang =", keliling)
	fmt.Println("Volume balok =", volume)

	fmt.Println("======= Soal 2 =======")
	/* Tulislah sebuah function dengan nama introduce yang menggunakan predefined value/ named value. pastikan semua parameter pada function introduce di gunakan semuanya */
	john := introduce("John", "laki-laki", "penulis", "30")
	fmt.Println(john) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"

	sarah := introduce("Sarah", "perempuan", "model", "28")
	fmt.Println(sarah) // Menampilkan "Bu Sarah adalah seorang model yang berusia 28 tahun"

	fmt.Println("======= Soal 3 =======")
	/* buatlah function yang menampung data slice di bawah ini sebagai variadic function */
	var buah = []string{"semangka", "jeruk", "melon", "pepaya"}

	var buahFavoritJohn = buahFavorit("John", buah...)

	fmt.Println(buahFavoritJohn)
	// halo nama saya john dan buah favorit saya adalah "semangka", "jeruk", "melon", "pepaya"

	fmt.Println("======= Soal 4 =======")
	/* buatlah closure function dengan nama tambahDataFilm untuk menambahkan data map[string]string ke slice dataFilm */
	var dataFilm = []map[string]string{}
	// buatlah closure function disini
	var tambahDataFilm = func(title, durasi, genre, tahun string) {
		dataFilm = append(dataFilm, map[string]string{
			"genre": genre,
			"jam":   durasi,
			"tahun": tahun,
			"title": title,
		})
	}
	tambahDataFilm("LOTR", "2 jam", "action", "1999")
	tambahDataFilm("avenger", "2 jam", "action", "2019")
	tambahDataFilm("spiderman", "2 jam", "action", "2004")
	tambahDataFilm("juon", "2 jam", "horror", "2004")

	for _, item := range dataFilm {
		fmt.Println(item)
	}
}
