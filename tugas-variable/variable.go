package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	/* tampilkan kalimat "Bootcamp Digital Skill Sanbercode Golang" yang tersusun dari gabungan variabel dalam setiap kata (5 variabel) */
	var (
		kata1 = "Bootcamp "
		kata2 = "Digital "
		kata3 = "Skill "
		kata4 = "Sanbercode "
		kata5 = "Golang "
	)
	fmt.Println("Jawaban Soal 1")
	fmt.Println(kata1 + kata2 + kata3 + kata4 + kata5)

	/* terdapat variabel seperti di bawah ini:
	halo := "Halo Dunia"
	ganti kata "Dunia" menjadi "Golang" menggunakan packages strings */
	halo := "Halo Dunia"
	newHalo := strings.Replace(halo, "Dunia", "Golang", -1)
	fmt.Println("Jawaban Soal 2")
	fmt.Println(newHalo)

	/* buatlah variabel-variabel seperti di bawah ini
	var kataPertama = "saya";
	var kataKedua = "senang";
	var kataKetiga = "belajar";
	var kataKeempat = "golang";
	gabungkan variabel-variabel tersebut agar menghasilkan output
	saya Senang belajaR GOLANG */
	var (
		kataPertama = "saya"
		kataKedua   = "senang"
		kataKetiga  = "belajar"
		kataKeempat = "golang"
	)
	gabunganKata := strings.ToLower(kataPertama) + " " + strings.Replace(kataKedua, "s", "S", -1) + " " + strings.Replace(kataKetiga, "r", "R", 1) + " " + strings.ToUpper(kataKeempat)
	fmt.Println("Jawaban Soal 3")
	fmt.Println(gabunganKata)

	/* buatlah variabel-variabel seperti di bawah ini
	var angkaPertama= "8";
	var angkaKedua= "5";
	var angkaKetiga= "6";
	var angkaKeempat = "7";
	ubahlah variabel diatas menjadi integer dan jumlahkan semuanya */
	var (
		angkaPertama = "8"
		angkaKedua   = "5"
		angkaKetiga  = "6"
		angkaKeempat = "7"
	)
	delapan, _ := strconv.Atoi(angkaPertama)
	lima, _ := strconv.Atoi(angkaKedua)
	enam, _ := strconv.Atoi(angkaKetiga)
	tujuh, _ := strconv.Atoi(angkaKeempat)
	jumlah := delapan + lima + enam + tujuh
	fmt.Println("Jawaban Soal 4")
	fmt.Println(jumlah)

	/* terdapat variabel seperti di bawah ini:*/
	kalimat := "halo halo bandung"
	angka := 2021
	stringAngka := strconv.Itoa(angka)
	/*olah variabel diatas yang hasil output akhrinya adalah
	"Hi Hi bandung" - 2021 */

	newKalimat := `"` + strings.Replace(kalimat, "halo", "Hi", -1) + `"` + " - " + stringAngka
	fmt.Println("Jawaban Soal 5")
	fmt.Println(newKalimat)
}
