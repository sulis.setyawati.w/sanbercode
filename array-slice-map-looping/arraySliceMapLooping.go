package main

import "fmt"

func main() {
	// Array
	// Deklarasi Array
	var fruits [5]string
	fruits[0] = "Anggur"
	fruits[1] = "Belimbing"
	fruits[2] = "Cempedak"
	fruits[3] = "Durian"
	fruits[4] = "Duku"
	fmt.Println(" List Buah ")
	for idx, val := range fruits {
		fmt.Println(idx+1, val)
	}
	// Inisialisasi nilai Array
	var vegetables = [2]string{"Spinach", "Brocoli"}
	fmt.Println(" List Sayur ")
	for i, j := range vegetables {
		fmt.Println(i+1, j)
	}

	//inisialisasi array tanpa jumlah element
	var seeds = [...]string{"corn", "rice", "sun flower", "chia"}
	fmt.Println(" List Biji Bijian ")
	for n, m := range seeds {
		fmt.Println(n+1, m)
	}

	//mengganti nilai Array
	fruits[0] = "buah diganti"
	seeds[len(seeds)-1] = "Sayuran terakhir diganti"
	fmt.Println("Array buah yang di ganti :", fruits)
	fmt.Println("Array biji bijian yang di ganti :", seeds)

	// Slice : Seperti array tapi tidak perlu mendeklarasikan jumlah elementnya
	//deklarasi slice
	var prima []int
	// inisialisasi nilai slice
	var ganjil = []int{1, 3, 5, 7, 9}
	// menambahkan data ke slice
	prima = append(prima, 2, 3, 5, 7)
	fmt.Println("=== Slice ===")
	fmt.Println("prima:", prima)
	fmt.Println("ganjil", ganjil)
	//Mengakses nilai slice
	fmt.Println("ganjil[0:2]", ganjil[0:2])
	fmt.Println("ganjil[2]", ganjil[2])

	// Slice adalah tipe data reference jika ada slice baru yang dibuat dari slice yang sudah ada, maka data nya akan mempunyai alamat memori yang sama
	newGanjil := ganjil[0:3]
	fmt.Println("newGanjil:", newGanjil)
	newGanjil[0] = 12
	fmt.Println("newGanjil", newGanjil)
	fmt.Println("ganjil", ganjil)

	// Operasi pada slice
	// fungsi cap : mengetahui capasitas maksimum slice
	fmt.Println("cap of ganjil", cap(ganjil))
	fmt.Println("len of ganjil", len(ganjil))
	fmt.Println("cap of newGanjil", cap(newGanjil))
	fmt.Println("len of newGanjil", len(newGanjil))

	// fungsi append : menambahkan data ke slice
	ganjil = append(ganjil, 11, 13, 15)
	fmt.Println("ganjil setelah append: ", ganjil)

	//fungsi copy : digunakan untuk men-copy elements slice pada src (parameter ke-2), ke dst (parameter pertama)
	a := []int{1, 2, 3, 4, 5}
	b := []int{15, 25, 35, 45, 55}
	var c = []int{10, 10}
	// data yang ada di c sebelumnya akan di replace
	_ = copy(c, a)
	_ = copy(b, c)
	fmt.Println(c)
	fmt.Println(b)

	/* MAP */
	// Deklarasi map
	var bulan = map[string]int{}
	// mengisi nilai map
	bulan["Januari"] = 31
	bulan["Febuari"] = 28
	bulan["Maret"] = 31
	fmt.Println(bulan)
	// menghapus item map
	delete(bulan, "Febuari")
	fmt.Println("bulan febuari dihapus :", bulan)

	// Deteksi Keberadaan Item Dengan Key Tertentu
	var value, isExist = bulan["Maret"]
	if isExist {
		fmt.Println("ada bulan maret", value)
	} else {
		fmt.Println("tidak ada bulan maret")
	}

	/* Looping */
	fmt.Println("======= Looping ========")
	// menggunakan for
	for i := 0; i < 10; i++ {
		fmt.Println(i, " kuadratnya ", i*i)
	}

	// penulisan alinnya
	no := 1
	for no <= 10 {
		fmt.Println(no, "looping")
		no++
	}

	// menggunakan for tanpa argumen => menggunakan break
	for {
		fmt.Println("looping selanjutnya", no)
		if no == 15 {
			break
		}
		no++
	}

	// Perulangan menggunakan for range
	for i, j := range bulan {
		fmt.Println(i, j)
	}

	// Penggunaaan Break and Continue
	for i := 0; i < 10; i++ {
		if i%2 == 0 {
			continue
		}
		if i == 5 {
			break
		}
		fmt.Println("Angka i :", i)
	}
}
