package main

import "fmt"

type (
	buah struct {
		nama       string
		warna      string
		adaBijinya bool
		harga      int
	}

	segitiga struct {
		alas, tinggi int
	}

	persegi struct {
		sisi int
	}

	persegiPanjang struct {
		panjang, lebar int
	}

	phone struct {
		name, brand string
		year        int
		colors      []string
	}

	movie struct {
		title, genre   string
		duration, year int
	}
)

//fungsi untuk menambah data buah ke []buah
func tambahBuah(b *[]buah, namaBuah, warnaBuah string, adaBijiBuahnya bool, hargaBuah int) {
	*b = append(*b, buah{
		nama:       namaBuah,
		warna:      warnaBuah,
		adaBijinya: adaBijiBuahnya,
		harga:      hargaBuah,
	})
}

func (s segitiga) hitungLuasSegitiga() float64 {
	luas := float64(s.alas * s.tinggi / 2)
	return luas
}

func (p persegi) hitungLuasPersegi() int {
	return p.sisi * p.sisi
}

func (pp persegiPanjang) hitungLuasPersegiPanjang() int {
	return pp.panjang * pp.lebar
}

func main() {
	fmt.Println("======= Soal 1 =======")
	/* Soal 1
	Buatlah sebuah struct dengan nama buah yang terdiri dari property nama dan warna tipe data string, lalu property adaBijinya dengan tipe data boolean, lalu property harga dengan tipe datanya integer. */
	var dataBuah []buah
	tambahBuah(&dataBuah, "Nanas", "Kuning", false, 9000)
	tambahBuah(&dataBuah, "Jeruk", "Oranye", true, 8000)
	tambahBuah(&dataBuah, "Semangka", "Hijau & Merah", true, 10000)
	tambahBuah(&dataBuah, "Pisang", "Kuning", false, 5000)
	fmt.Println(dataBuah)

	fmt.Println("======= Soal 2 =======")
	/* Soal 2
	buatlah method luas untuk masing2 struct, print hasil penggunaan methodnya */
	sikuSiku := segitiga{6, 8}
	luasSegitiga := sikuSiku.hitungLuasSegitiga()
	fmt.Println("Luas segitiga siku siku =", luasSegitiga)

	persegiABCD := persegi{5}
	fmt.Println("luas persegi ABCD =", persegiABCD.hitungLuasPersegi())

	persegiPanjangPQRS := persegiPanjang{10, 5}
	fmt.Println("luas persegi panjang PQRS =", persegiPanjangPQRS.hitungLuasPersegiPanjang())

	fmt.Println("======= Soal 3 =======")
	/* buatlah method yang menambahkan warna di property colors pada sebuah object dari phone (jika memerlukan pointer silahkan gunakan pointer) */
	var dataPhone []phone
	samsung := phone{
		name:   "Galaxy A02",
		brand:  "Samsung",
		year:   2018,
		colors: []string{"Hitam"},
	}
	dataPhone = append(dataPhone, samsung)
	samsung.tambahWarna(&dataPhone, samsung, "Biru")
	samsung.tambahWarna(&dataPhone, samsung, "Kuning")
	fmt.Println(dataPhone)

	fmt.Println("======= Soal 4 =======")
	/* buatlah struct movie dengan property title dan genre tipe datanya string, lalu duration dan year tipe datanya integer
	lalu buatlah function dengan nama tambahDataFilm untuk menambahkan data object dari struct ke slice dataFilm, lalu tampilkan datanya sesuai output yang di inginkan (pada soal ini memerlukan pointer)
	*/
	var dataFilm = []movie{}
	tambahDataFilm("LOTR", 120, "action", 1999, &dataFilm)
	tambahDataFilm("avenger", 120, "action", 2019, &dataFilm)
	tambahDataFilm("spiderman", 120, "action", 2004, &dataFilm)
	tambahDataFilm("juon", 120, "horror", 2004, &dataFilm)
	for idx, val := range dataFilm {
		fmt.Print(idx+1, ".")
		fmt.Println("\ttitle: ", val.title)
		fmt.Println("\tduration: ", val.duration)
		fmt.Println("\tgenre: ", val.genre)
		fmt.Println("\tyear: ", val.year)

	}

}

func tambahDataFilm(judul string, durasi int, genreFilm string, tahun int, df *[]movie) {
	*df = append(*df, movie{
		title:    judul,
		duration: durasi,
		genre:    genreFilm,
		year:     tahun,
	})
}

func (ph phone) tambahWarna(dataPhone *[]phone, data phone, warna string) {
	var dp = *dataPhone
	for idx, val := range *dataPhone {
		if val.name == data.name && val.brand == data.brand && val.year == data.year {
			dp[idx].colors = append(dp[idx].colors, warna)
		}
	}
	dataPhone = &dp
}
