package main

import "fmt"

func main() {
	/* Operator di golang */
	// Operator matematika
	a := 21
	b := 5
	penjumlahan := a + b
	pengurangan := a - b
	perkalian := a * b
	pembagian := a / b
	modulus := a % b
	fmt.Println("hasil operasi aritmatika antara bilangan 21 dan 5 di golang")
	fmt.Println("penjumlahan => ", penjumlahan)
	fmt.Println("pengurangan => ", pengurangan)
	fmt.Println("perkalian => ", perkalian)
	fmt.Println("pembagian => ", pembagian)
	fmt.Println("modulo => ", modulus)

	// augmented assignment
	a += a
	fmt.Println("augmented assignment of a = 21")
	fmt.Println("a += a =>", a)
	a = 21
	a -= a
	fmt.Println("a -= a =>", a)
	a = 21
	a *= a
	fmt.Println("a *= a =>", a)
	a = 21
	a /= 21
	fmt.Println("a /= a =>", a)
	a = 21
	a %= a
	fmt.Println("a %= a =>", a)

	// Operator perbandingan
	fmt.Println("Operator Perbandingan:")
	fmt.Println("5 > 3 bernilai", 5 > 3)
	fmt.Println("5 < 3 bernilai", 5 < 3)
	fmt.Println("5 == 3 bernilai", 5 == 3)
	fmt.Println("5 != 3 bernilai", 5 != 3)
	fmt.Println("5 >= 3 bernilai", 5 >= 3)
	fmt.Println("5 <= 3 bernilai", 5 <= 3)

	// Operator Logika
	fmt.Println("Operator Logika:")
	fmt.Println("5 >= 3 && 5 > 3 bernilai", 5 >= 3 && 5 > 3)
	fmt.Println("5 >= 3 && 5 < 3 bernilai", 5 >= 3 && 5 < 3)
	fmt.Println("5 >= 3 || 5 < 3 bernilai", 5 >= 3 || 5 < 3)
	fmt.Println("!(5 >= 3) bernilai", !(5 >= 3))

	/* Conditional */
	// if, else & if else
	fmt.Println("Conditional menggunakan if else")
	nilai := 85
	fmt.Println("nilai ujian =", nilai)
	if nilai > 75 {
		fmt.Println("Lulus")
	} else {
		fmt.Println("Mengulang")
	}

	if nilai < 50 {
		fmt.Println("Index = E")
	} else if nilai < 70 {
		fmt.Println("Index = D")
	} else if nilai < 80 {
		fmt.Println("Index = C")
	} else if nilai < 90 {
		fmt.Println("Index = B")
	} else {
		fmt.Println("Index = A")
	}

	// Conditional Dengan Switch

	switch {
	case (nilai <= 50):
		fmt.Println("Index = E")
		fallthrough
	case (nilai < 70 && nilai > 50):
		fmt.Println("Index = D")
		fallthrough
	case (nilai < 80 && nilai >= 70):
		fmt.Println("Index = C")
		fallthrough
	case (nilai < 90 && nilai >= 80):
		fmt.Println("Index = B")
		fallthrough
	case (nilai <= 100):
		fmt.Println("Index = A")
		fallthrough
	case nilai >= 75:
		fmt.Println("lulus")
	}

}
