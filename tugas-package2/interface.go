package tugaspackage2

import (
	"strconv"
	"strings"
)

type SegitigaSamaSisi struct {
	Alas, Tinggi int
}

type PersegiPanjang struct {
	Panjang, Lebar int
}

type Tabung struct {
	JariJari, Tinggi float64
}

type Balok struct {
	Panjang, Lebar, Tinggi int
}

type HitungBangunDatar interface {
	Luas() int
	Keliling() int
}

type HitungBangunRuang interface {
	Volume() float64
	LuasPermukaan() float64
}

type Cetak interface {
	CetakData() string
}

type Phone struct {
	Name, Brand string
	Year        int
	Colors      []string
}

func (sss SegitigaSamaSisi) Luas() int {
	return sss.Alas * sss.Tinggi / 2
}

func (sss SegitigaSamaSisi) Keliling() int {
	return sss.Alas * 3
}

func (pp PersegiPanjang) Luas() int {
	return pp.Panjang * pp.Lebar
}

func (pp PersegiPanjang) Keliling() int {
	return 2 * (pp.Panjang + pp.Lebar)
}

func (b Balok) LuasPermukaan() float64 {
	return 2 * float64((b.Panjang*b.Lebar)+(b.Lebar*b.Tinggi)+(b.Tinggi*b.Panjang))
}

func (b Balok) Volume() float64 {
	return float64(b.Panjang * b.Lebar * b.Tinggi)
}

func (p Phone) CetakData() string {
	stringdata := "\nname\t: " + p.Name + "\nbrand\t:" + p.Brand + "\nyear\t:" + strconv.Itoa(p.Year) + "\ncolor\t:" + strings.Join(p.Colors, ",")
	return stringdata
}

// func main() {
// 	fmt.Println("======= Soal 1 =======")
// 	/* Soal 1
// 	gunakanlah interface bangunDatar untuk menentukan Luas dan Keliling berdasarkan struct SegitigaSamaSisi dan PersegiPanjang
// 	lalu gunakanlah interface bangunRuang untuk menentukan Volume dan Luas permukaan berdasarkan struct tabung dan Balok
// 	print semua hasil perhitungannya */
// 	var bangunDatar HitungBangunDatar = SegitigaSamaSisi{
// 		Alas:   10,
// 		Tinggi: 8,
// 	}
// 	fmt.Println("Keliling segitiga = ", bangunDatar.Keliling())
// 	fmt.Println("Luas segitiga = ", bangunDatar.Luas())

// 	bangunDatar = PersegiPanjang{
// 		Panjang: 12,
// 		Lebar:   6,
// 	}
// 	fmt.Println("Keliling persegi Panjang = ", bangunDatar.Keliling())
// 	fmt.Println("Luas persegi Panjang = ", bangunDatar.Luas())

// 	var bangunRuang HitungBangunRuang = Balok{
// 		Panjang: 10,
// 		Lebar:   8,
// 		Tinggi:  5,
// 	}
// 	fmt.Println("Luas permukaan Balok = ", bangunRuang.LuasPermukaan())
// 	fmt.Println("Volume Balok = ", bangunRuang.Volume())

// 	fmt.Println("======= Soal 2 =======")
// 	/* buatlah interface yang berisi method dengan tipe data string untuk menampilkan data (nama interface dan nama methodnya bebas)
// 	lalu tampilkan dengan output yang diinginkan kurang lebih seperti di bawah ini (output ini di simpan dalam method dari interface bukan di print satu-satu) */
// 	var samsung Cetak = Phone{
// 		name:   "Samsung Galxy Note 20",
// 		brand:  "Samsung",
// 		year:   2020,
// 		colors: []string{"Hitam", "Biru"},
// 	}
// 	fmt.Println("Data Hp Samsung\n", samsung.CetakData())

// 	fmt.Println("======= Soal 3 =======")
// 	/* buatlah function yang tipe data return-nya adalah interface kosong, dengan kondisi:
// 	jika parameter kedua bernilai true maka tampilkan kalimat (asumsi sisinya 2)"Luas persegi dengan sisi 2 cm adalah 4 cm"
// 	jika parameter kedua bernilai false maka tampilkan hanya hasil angka saja (misal 4)
// 	jika parameter pertama 0 dan parameter kedua bernilai true tampilkan "Maaf anda belum menginput sisi dari persegi"
// 	jika parameter pertama 0 dan parameter kedua bernilai false maka tampilkan nil */
// 	fmt.Println(LuasPersegi(4, true))
// 	fmt.Println(LuasPersegi(8, false))
// 	fmt.Println(LuasPersegi(0, true))
// 	fmt.Println(LuasPersegi(0, false))

// 	fmt.Println("======= Soal 4 =======")
// 	var prefix interface{} = "hasil penjumlahan dari "
// 	var kumpulanAngkaPertama interface{} = []int{6, 8}
// 	var kumpulanAngkaKedua interface{} = []int{12, 14}
// 	/* gunakan seluruh variabel tersebut untuk menghasilkan output "hasil penjumlahan dari 6 + 8 + 12 + 14 = 40" */
// 	var arrayAngkaPertama = kumpulanAngkaPertama.([]int)
// 	var arrayAngkaKedua = kumpulanAngkaKedua.([]int)
// 	var stringPrefix = prefix.(string)
// 	var jumlah int
// 	var angka []string
// 	for _, val := range arrayAngkaPertama {
// 		jumlah += val
// 		angka = append(angka, strconv.Itoa(val))
// 	}
// 	for _, val := range arrayAngkaKedua {
// 		jumlah += val
// 		angka = append(angka, strconv.Itoa(val))
// 	}
// 	result := stringPrefix + strings.Join(angka, " + ") + " = " + strconv.Itoa(jumlah)
// 	fmt.Println(result)

// }

func LuasPersegi(sisi int, cond bool) interface{} {
	var result interface{}
	switch {
	case sisi > 0 && cond:
		result = "Luas persegi dengan sisi " + strconv.Itoa(sisi) + " cm adalah " + strconv.Itoa(sisi*sisi) + " cm persegi"
	case sisi > 0 && !cond:
		result = sisi * sisi
	case sisi <= 0 && cond:
		result = "Maaf anda belum menginput sisi dari persegi dengan benar"
	case sisi <= 0 && !cond:
		result = nil
	}
	return result
}
