package main

import (
	"fmt"
	"strconv"
)

type (
	PersegiPanjang struct {
		panjang  int
		lebar    int
		keliling int
		luas     int
	}
	Segitiga struct {
		alas   int
		tinggi int
		luas   float64
	}
	nilaiSiswa struct {
		nama  string
		nilai int
		index string
	}
)

func main() {
	/* soal 1
	buatlah variabel-variabel seperti di bawah ini*/
	var panjangPersegiPanjang string = "8"
	var lebarPersegiPanjang string = "5"
	var alasSegitiga string = "6"
	var tinggiSegitiga string = "7"
	//ubah lah variabel diatas ke dalam integer dan gunakan pada operasi perhitungan dari luas persegi panjang, keliling persegi panjang dan luas segitiga dengan variabel di bawah ini:
	var luasPersegiPanjang int
	var kelilingPersegiPanjang int
	var luasSegitiga int
	/*lalu tampilkan hasil perhitungannya */
	fmt.Println("================= Soal 1 ==================")
	var PP PersegiPanjang
	PP.panjang, _ = strconv.Atoi(panjangPersegiPanjang)
	PP.lebar, _ = strconv.Atoi(lebarPersegiPanjang)
	PP.keliling = 2 * (PP.panjang + PP.lebar)
	PP.luas = PP.panjang * PP.lebar

	var S Segitiga
	S.alas, _ = strconv.Atoi(alasSegitiga)
	S.tinggi, _ = strconv.Atoi(tinggiSegitiga)
	S.luas = float64(S.alas) * float64(S.tinggi) / 2

	luasPersegiPanjang = PP.luas
	kelilingPersegiPanjang = PP.keliling
	luasSegitiga = int(S.luas)

	fmt.Println("Luas Persegi panjang =", luasPersegiPanjang)
	fmt.Println("Keliling Persegi panjang =", kelilingPersegiPanjang)
	fmt.Println("Luas Segitiga =", luasSegitiga)

	// Soal 2
	// buatlah variabel seperti di bawah ini
	var nilaiJohn = 80
	var nilaiDoe = 50
	// tentukan indeks nilai dari nilaiJohn dan nilaiDoe (tampilkan dengan fmt.Println) dengan kondisi :
	// nilai >= 80 indeksnya A
	// nilai >= 70 dan nilai < 80 indeksnya B
	// nilai >= 60 dan nilai < 70 indeksnya c
	// nilai >= 50 dan nilai < 60 indeksnya D
	// nilai < 50 indeksnya E
	// kerjakan soal ini tanpa menggunakan function(ini materi hari 5)
	fmt.Println("================= Soal 2 =================")
	var daftarNilai []nilaiSiswa
	daftarNilai = append(daftarNilai, nilaiSiswa{
		nama:  "John",
		nilai: nilaiJohn,
	},
		nilaiSiswa{
			nama:  "Doe",
			nilai: nilaiDoe,
		})

	for _, val := range daftarNilai {
		fmt.Println("Nilai ", val.nama, " adalah ", val.nilai)
		switch {
		case val.nilai >= 80:
			fmt.Println("Index = A")
		case val.nilai >= 70:
			fmt.Println("Index = B")
		case val.nilai >= 60:
			fmt.Println("Index = C")
		case val.nilai >= 50:
			fmt.Println("Index = D")
		case val.nilai < 50:
			fmt.Println("Index = E")
		}
	}

	// Soal 3
	// buatlah variabel seperti di bawah ini
	var tanggal = 17
	var bulan = 8
	var tahun = 1945
	var stringBulan string
	var stringTanggal = strconv.Itoa(tanggal)
	var stringTahun = strconv.Itoa(tahun)
	// ganti tanggal ,bulan, dan tahun sesuai dengan tanggal lahir anda dan buatlah switch case pada bulan, lalu muncul kan string nya dengan output seperti ini 17 Agustus 1945 (isi di sesuaikan dengan tanggal lahir masing-masing)
	fmt.Println("================= Soal 3 =================")
	switch bulan {
	case 1:
		stringBulan = "Januari"
	case 2:
		stringBulan = "Febuari"
	case 3:
		stringBulan = "Maret"
	case 4:
		stringBulan = "April"
	case 5:
		stringBulan = "Mei"
	case 6:
		stringBulan = "juni"
	case 7:
		stringBulan = "Juli"
	case 8:
		stringBulan = "Agustus"
	case 9:
		stringBulan = "September"
	case 10:
		stringBulan = "Oktober"
	case 11:
		stringBulan = "November"
	case 12:
		stringBulan = "Desember"
	}
	fmt.Println(stringTanggal, stringBulan, stringTahun)

	fmt.Println("================= Soal 4 =================")
	// Berikut adalah beberapa istilah generasi berdasarkan tahun kelahirannya:
	// Baby boomer, kelahiran 1944 s.d 1964
	// Generasi X, kelahiran 1965 s.d 1979
	// Generasi Y (Millenials), kelahiran 1980 s.d 1994
	// Generasi Z, kelahiran 1995 s.d 2015
	// buatlah conditional dimana menghasilkan istilah diatas sesuai dengan tahun kelahiran anda
	var tahunLahir = 1996
	var generasi string
	switch {
	case (tahunLahir >= 1944 && tahunLahir <= 1964):
		generasi = "Baby Boomer"
	case (tahunLahir > 1964 && tahunLahir <= 1979):
		generasi = "X"
	case (tahunLahir > 1979 && tahunLahir <= 1994):
		generasi = "Y"
	case (tahunLahir > 1994 && tahunLahir <= 2015):
		generasi = "Z"
	}
	fmt.Println("kelahiran tahun ", tahunLahir, " adalah generasi ", generasi)
}
