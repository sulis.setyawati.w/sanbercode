package main

import (
	"fmt"
	tp "sanbercode/tugas-package2"
	"strconv"
	"strings"
)

func main() {
	var bangunDatar tp.HitungBangunDatar = tp.SegitigaSamaSisi{Alas: 10, Tinggi: 10}

	fmt.Println("Keliling segitiga = ", bangunDatar.Keliling())
	fmt.Println("Luas segitiga = ", bangunDatar.Luas())

	bangunDatar = tp.PersegiPanjang{
		Panjang: 12,
		Lebar:   6,
	}
	fmt.Println("Keliling persegi Panjang = ", bangunDatar.Keliling())
	fmt.Println("Luas persegi Panjang = ", bangunDatar.Luas())

	var bangunRuang tp.HitungBangunRuang = tp.Balok{
		Panjang: 10,
		Lebar:   8,
		Tinggi:  5,
	}
	fmt.Println("Luas permukaan Balok = ", bangunRuang.LuasPermukaan())
	fmt.Println("Volume Balok = ", bangunRuang.Volume())

	fmt.Println("======= Soal 2 =======")
	/* buatlah interface yang berisi method dengan tipe data string untuk menampilkan data (nama interface dan nama methodnya bebas)
	lalu tampilkan dengan output yang diinginkan kurang lebih seperti di bawah ini (output ini di simpan dalam method dari interface bukan di print satu-satu) */
	var samsung tp.Cetak = tp.Phone{
		Name:   "Samsung Galaxy Note 20",
		Brand:  "Samsung",
		Year:   2020,
		Colors: []string{"Hitam", "Biru"},
	}
	fmt.Println("Data Hp Samsung\n", samsung.CetakData())

	fmt.Println("======= Soal 3 =======")
	/* buatlah function yang tipe data return-nya adalah interface kosong, dengan kondisi:
	jika parameter kedua bernilai true maka tampilkan kalimat (asumsi sisinya 2)"Luas persegi dengan sisi 2 cm adalah 4 cm"
	jika parameter kedua bernilai false maka tampilkan hanya hasil angka saja (misal 4)
	jika parameter pertama 0 dan parameter kedua bernilai true tampilkan "Maaf anda belum menginput sisi dari persegi"
	jika parameter pertama 0 dan parameter kedua bernilai false maka tampilkan nil */
	fmt.Println(tp.LuasPersegi(4, true))
	fmt.Println(tp.LuasPersegi(8, false))
	fmt.Println(tp.LuasPersegi(0, true))
	fmt.Println(tp.LuasPersegi(0, false))

	fmt.Println("======= Soal 4 =======")
	var prefix interface{} = "hasil penjumlahan dari "
	var kumpulanAngkaPertama interface{} = []int{6, 8}
	var kumpulanAngkaKedua interface{} = []int{12, 14}
	/* gunakan seluruh variabel tersebut untuk menghasilkan output "hasil penjumlahan dari 6 + 8 + 12 + 14 = 40" */
	var arrayAngkaPertama = kumpulanAngkaPertama.([]int)
	var arrayAngkaKedua = kumpulanAngkaKedua.([]int)
	var stringPrefix = prefix.(string)
	var jumlah int
	var angka []string
	for _, val := range arrayAngkaPertama {
		jumlah += val
		angka = append(angka, strconv.Itoa(val))
	}
	for _, val := range arrayAngkaKedua {
		jumlah += val
		angka = append(angka, strconv.Itoa(val))
	}
	result := stringPrefix + strings.Join(angka, " + ") + " = " + strconv.Itoa(jumlah)
	fmt.Println(result)
}
