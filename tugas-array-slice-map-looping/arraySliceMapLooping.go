package main

import (
	"fmt"
)

func main() {
	/*
		Soal 1
		A. Jika angka ganjil maka tampilkan Santai
		B. Jika angka genap maka tampilkan Berkualitas
		C. Jika angka yang sedang ditampilkan adalah kelipatan 3 DAN angka ganjil maka tampilkan I Love Coding. */

	fmt.Println("======= Soal 1 =======")
	var kalimat string
	for i := 1; i <= 20; i++ {
		if i%2 == 0 {
			kalimat = "Berkualitas"
		} else {
			switch {
			case i%3 != 0:
				kalimat = "Santai"
			case i%3 == 0:
				kalimat = "I Love Coding"
			}
		}
		fmt.Println(i, "-", kalimat)
	}

	/*
		Soal 2
		Kali ini kamu diminta untuk menampilkan sebuah segitiga dengan tanda pagar (#) dengan dimensi tinggi 7 dan alas 7. gunakan looping untuk mengerjakan soal ini */
	fmt.Println("======= Soal 2 =======")
	for a := 0; a < 7; a++ {
		fmt.Println()
		for b := 0; b < 7; b++ {
			if a >= b {
				fmt.Print(" # ")
			}
		}
	}
	fmt.Println()

	var kalimat2 = [...]string{"aku", "dan", "saya", "sangat", "senang", "belajar", "golang"}
	/*olah variabel diatas agar menghasilkan output seperti dibawah ini
	[saya sangat senang belajar golang]*/
	fmt.Println("======= Soal 3 =======")
	newKalimat2 := kalimat2[2:]
	fmt.Println(newKalimat2)

	/* buatlah variabel seperti di bawah ini
	var sayuran = []string{}
	tambahkanlah data di bawah ini ke variabel sayuran:
	Bayam
	Buncis
	Kangkung
	Kubis
	Seledri
	Tauge
	Timun
	lalu tampilkan dengan loop dan beri angka di depannya */
	fmt.Println("======= Soal 4 =======")
	var sayuran = []string{}
	sayuran = append(sayuran, "Bayam", "Buncis", "Kangkung", "Kubis", "Seledri", "Tauge", "Timun")
	for idx, item := range sayuran {
		fmt.Print(idx+1, ".")
		fmt.Println(item)
	}

	/*gunakanlah variabel diatas menggunakan looping untuk menghasilkan output */
	var satuan = map[string]int{
		"panjang": 7,
		"lebar":   4,
		"tinggi":  6,
	}
	fmt.Println("======= Soal 5 =======")
	for key, value := range satuan {
		fmt.Print(key, " = ")
		fmt.Println(value)
	}
}
