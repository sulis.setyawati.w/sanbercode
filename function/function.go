package main

import (
	"fmt"
	"strconv"
)

// Fungsi sederhana
func printHello() {
	fmt.Println("Halo, kali ini kita belajar function")
}

// fungsi dengan parameter
func hitungPenjumlahan(bil1, bil2 int) {
	hasilPenjumlahan := bil1 + bil2
	fmt.Println("Hasil penjumlahan dari ", bil1, " dan ", bil2, " adalah ", hasilPenjumlahan)
}

//fungsi dengan return value
func hitungPerkalian(bil1, bil2 int) int {
	hasilPerkalian := bil1 * bil2
	return hasilPerkalian
}

// fungsi dengan multiple return value
func perkenalan() (string, string) {
	namaDepan := "Sulis"
	namaBelakang := "Winingsih"
	return namaDepan, namaBelakang
}

// Function Predefined return value
func tambahAngka(angka int) (listAngka []int) {
	listAngka = append(listAngka, angka)
	return
}

// Variadic Function : pembuatan fungsi dengan parameter tak terbatas
func tambahBanyakAngka(angka ...int) (listAngka []int) {
	listAngka = append(listAngka, angka...)
	return
}

// kombinasi parameter variadic dengan parameter biasa
func cetakMenu(kategori string, varian ...string) {
	fmt.Println("ini kategori:", kategori)
	fmt.Println("ini contoh variannya:", varian)
}

// function yang dijadikan paramter
func convertStringToInt(angka string) (bilangan int) {
	bilangan, _ = strconv.Atoi(angka)
	return
}

// function yang mempunyai parameter fungsi
func tambahAngkaDariString(angka func(string) int, listAngka []int) []int {
	listAngka = append(listAngka, angka("19"))
	return listAngka
}

func main() {
	// cara memanggil funsi
	printHello()

	//memanggil fungsi dengan parameter tanpa return value
	hitungPenjumlahan(10, 20)

	//memanggil fungsi dengan return value
	a := hitungPerkalian(5, 7)
	fmt.Println("hasil perkalian =", a)

	// fungsi dengan multiple return value
	namaDepan, namaBelakang := perkenalan()
	fmt.Println("Halo perkenalkan nama saya ", namaDepan, namaBelakang)

	// Function Predefined return value
	listAngka := tambahAngka(5)
	fmt.Println(listAngka)

	// variadic function
	listAngka = tambahBanyakAngka(5, 7, 9)
	fmt.Println("variadic fuhsi basic : ", listAngka)

	// Pengisian Parameter Fungsi Variadic Menggunakan Data Slice
	numbers := []int{11, 13, 15, 17}
	listAngka = tambahBanyakAngka(numbers...)
	fmt.Println("funsi variadic dengan data slice :", listAngka)

	// kombinasi parameter biasa dengan variadic
	kategori := []string{"buah", "sayur", "umbi"}
	listBuah := []string{"Apel", "Anggur", "Alpukat"}
	cetakMenu(kategori[0], listBuah...)

	// Closure :  sebuah fungsi yang bisa disimpan dalam variabel
	// Closure merupakan anonymous function atau fungsi tanpa nama
	// Biasa dimanfaatkan untuk membungkus suatu proses yang hanya dipakai sekali atau dipakai pada blok tertentu saja.
	// Closure disimpan dalam sebuah variable
	var hitungBanyakAngka = func(listAngka []int) int {
		return len(listAngka)
	}
	banyakAngka := hitungBanyakAngka(listAngka)
	fmt.Println("banyaknya angka adalah ", banyakAngka)

	// Closure sebagai return value
	var findMax = func(listAngka []int) func() int {
		var numb int
		for _, val := range listAngka {
			if val > numb {
				numb = val
			}
		}
		return func() int {
			return numb
		}
	}

	numb := findMax(listAngka)
	fmt.Println(numb())

	// Function sebagai parameter
	listAngka = tambahAngkaDariString(convertStringToInt, listAngka)
	fmt.Println("list angka baru : ", listAngka)
}
