package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	// cara deklarasi variable di golang
	// dengan var dan tipe datanya, bisa dengan atau tanpa nilai awalnya sekaligus
	var kata string
	var kata2 string = "Halo dunia"
	// dengan var tanpa menambahkan tipe data
	var kata3 = "Halo semua, hari ini kita akan belajar golang"
	// dengan ':=' dan harus menyertakan nilai awal
	kata4 := "pertama kita belajar bagaimana mendeklarasikan variable"

	// Cara mengganti nilai variable
	// cukup dengan memanggil varibale dan mengganti nilainya dengan '='
	kata = "kata pertama"
	kata2 = "kata kedua"
	kata3 = "kata ketiga"
	kata4 = "kata keempat"

	// mencetak nilai variable
	fmt.Println("mencetak variable kata")
	fmt.Println(kata)
	fmt.Println(kata2)
	fmt.Println(kata3)
	fmt.Println(kata4)

	// Konversi tipe data dengan casting
	var desimal float64 = float64(24)
	fmt.Println("hasil konversi = ", desimal)

	// menggunakan pacakage strings
	// strings.index => digunakan untuk mencari posisi index sebuah string dalam data string
	var kalimat = "kita sedang belajar golang dari dasar"
	var indexGolang = strings.Index(kalimat, "golang")
	fmt.Println("dalam kalimat (", kalimat, ") kata golang posisi indexnya adalah ", indexGolang)
	fmt.Println("dalam kalimat (", kalimat, ") huruf xyz posisi indexnya adalah ", strings.Index(kalimat, "xyz"))
	fmt.Println("dalam kalimat (", kalimat, ") huruf a posisi indexnya adalah ", strings.Index(kalimat, "a"))

	// strings.Replace() => untuk menggantin string dengan string tertentu
	var kalimat1 = "ada 2 kambing 2 sapi dan 2 kerbau"
	fmt.Println("angka 2 dalam (", kalimat1, ")diganti dengan dua menjadi :", strings.Replace(kalimat1, "2", "dua", 1))
	fmt.Println("angka 2 dalam (", kalimat1, ")diganti dengan dua menjadi :", strings.Replace(kalimat1, "2", "dua", 2))
	fmt.Println("angka 2 dalam (", kalimat1, ")diganti dengan dua menjadi :", strings.Replace(kalimat1, "2", "dua", -1))

	// strings.Repeat() => untuk membuat string dari hasil perulangan strings
	str := strings.Repeat("la ", 3)
	fmt.Println("hasil perulangan la :", str)

	// strings.ToLower => untuk mengubah semua strings menjadi huruf kecil
	kalimat2 := "HURUF Kecil SEMUA"
	fmt.Println("kalimat dengan huruf kecil semua :", strings.ToLower(kalimat2))
	fmt.Println("kalimat dengan huruf besar semua :", strings.ToUpper(kalimat2))

	// menggunakan package strconv
	// strconv.Atoi() => mengubah string ke angka
	stringAngka := "123"
	angka, _ := strconv.Atoi(stringAngka)
	fmt.Println("kuadrat angka 123 = ", angka*angka)
	angkaSembarang, _ := strconv.Atoi("A")
	fmt.Println("int dari string sembarang (A) =", angkaSembarang)

	// strconv.Itoa() => mengubah int ke string
	angka = 2022
	stringAngka = strconv.Itoa(angka)
	fmt.Println("code ini dibuat tahun " + stringAngka)

	// strconv.ParseInt() => mengubah string numerek ke int dengan basis tertentu
	angkaBasis, _ := strconv.ParseInt(stringAngka, 8, 64)
	fmt.Println("hasil strconv.ParseInt =", angkaBasis)
}
