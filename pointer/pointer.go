package main

import (
	"fmt"
	"strconv"
)

type biodata struct {
	nama string
	usia int
}

func main() {
	// Pointer adalah reference atau alamat memori
	//mengambil nilai pointer dari variable biasa
	var dataSiswa biodata
	// deklarasi pointer
	// mengambil nilai pointer dari data biasa dengan &dataSiswa
	var dataSiswaPointer *biodata = &dataSiswa

	dataSiswa.nama = "Sofi"
	dataSiswa.usia = 15

	fmt.Println(dataSiswa)
	fmt.Println(dataSiswaPointer)

	//ubah datanya : dataSiswaPointer ikut berubah karena nilai variable refernce nya berubah
	dataSiswa.nama = "Sofia"
	dataSiswa.usia = 16

	fmt.Println(dataSiswa)
	fmt.Println(dataSiswaPointer)

	// Mengakses nilai reference dengan *
	dataDiri := *dataSiswaPointer
	fmt.Println(dataDiri)

	// menggunakan pointer sebagai parameter
	kalimatPerkenalan := perkenalan(dataSiswaPointer)
	fmt.Println(kalimatPerkenalan)

	// Jika dataSiswa diubah maka dataSiswaPointer juga ikut berubah
	dataSiswa.nama = "Julian"
	dataSiswa.usia = 13

	kalimatPerkenalan = perkenalan(dataSiswaPointer)
	fmt.Println(kalimatPerkenalan)

	//dataSiswaPointer diubah
	dataSiswaPointer.nama = "Sulis"
	dataSiswaPointer.usia = 26
	kalimatPerkenalan = perkenalan(dataSiswaPointer)
	fmt.Println(kalimatPerkenalan)
	fmt.Println(dataSiswa)

}

func perkenalan(dataDiri *biodata) string {
	kalimatPerkenalan := "Halo, nama saya " + dataDiri.nama + " saya berusia " + strconv.Itoa(dataDiri.usia) + " tahun"
	return kalimatPerkenalan
}
