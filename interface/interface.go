package main

import (
	"fmt"
	"math"
)

// Deklarasi Interface
type (
	hitung2d interface {
		luas() float64
		keliling() float64
	}

	hitung3d interface {
		volume() float64
	}

	lingkaran struct {
		diameter float64
	}

	persegi struct {
		sisi float64
	}

	kubus struct {
		sisi float64
	}

	/* 2. Embedded Interface
	Interface bisa di-embed ke interface lain, sama seperti struct. Cara penerapannya juga sama, cukup dengan menuliskan nama interface yang ingin di-embed ke dalam interface tujuan.
	*/
	hitung interface {
		hitung2d
		hitung3d
	}
)

func (l lingkaran) jariJari() float64 {
	return l.diameter / 2
}

func (l lingkaran) luas() float64 {
	return math.Pi * math.Pow(l.jariJari(), 2)
}

func (l lingkaran) keliling() float64 {
	return math.Pi * l.diameter
}

func (p persegi) luas() float64 {
	return math.Pow(p.sisi, 2)
}

func (p persegi) keliling() float64 {
	return p.sisi * 4
}

func (k kubus) volume() float64 {
	return math.Pow(k.sisi, 3)
}

func (k kubus) keliling() float64 {
	return k.sisi * 12
}

func (k kubus) luas() float64 {
	return k.sisi * k.sisi * 6
}

func main() {
	var bangunDatar hitung2d = persegi{10.0}
	fmt.Println("===== hasil hitung2d persegi =====")
	fmt.Println("Luas = ", bangunDatar.luas())
	fmt.Println("Keliling = ", bangunDatar.keliling())

	bangunDatar = lingkaran{7.0}
	fmt.Println("===== hasil hitung2d lingkaran =====")
	fmt.Println("Luas = ", bangunDatar.luas())
	fmt.Println("Keliling = ", bangunDatar.keliling())
	fmt.Println("Jari - jari = ", bangunDatar.(lingkaran).jariJari())

	//menggunakan embeded interface
	var bangunRuang hitung = kubus{6}
	fmt.Println("===== hasil hitung3d kubus =====")
	fmt.Println("volume = ", bangunRuang.volume())

	// Interface kosong : interface{}
	/* Interface kosong atau empty interface yang dinotasikan dengan interface{} merupakan tipe data yang sangat spesial. Variabel bertipe ini bisa menampung segala jenis data, bahkan array, pointer, apapun */

	var ik interface{}
	ik = "isi interface dengan string"
	fmt.Println(ik)
	ik = [3]int{1, 2, 3}
	fmt.Println(ik)

	// casting interface untuk mendapatkan nilainya
	// casting ke array
	arrayIk := ik.([3]int)
	fmt.Println(arrayIk)
	fmt.Println(arrayIk[1])

	// casting interface kosong ke pointer
	ik = &kubus{sisi: 12.0}
	pointerIk := ik.(*kubus)
	fmt.Println(ik)
	fmt.Println(pointerIk)

}
