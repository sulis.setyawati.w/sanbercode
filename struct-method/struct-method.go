package main

import "fmt"

// Dekalarasi struct

type (

	// embedded struct : mekanisme untuk menempelkan sebuah struct sebagai properti struct lain
	exam struct {
		subject string
		score   int
		student
	}

	student struct {
		name  string
		grade int
	}

	person struct {
		name string
		age  int
	}

	// Nested struct
	/* Nested struct adalah anonymous struct yang di-embed ke sebuah struct. Deklarasinya langsung didalam struct peng-embed. Contoh: */
	student2 struct {
		person2 struct {
			name string
			age  int
		}
		grade int
	}
)

func main() {
	// cara menggunakan struct
	var Nadia student
	Nadia.name = "Nadia Putri"
	Nadia.grade = 11
	fmt.Println("Nama\t:", Nadia.name)
	fmt.Println("Grade\t:", Nadia.grade)

	// cara lain membuat data struct
	//mengisikan data langsung dan harus berurutan
	John := student{"John Doe", 11}
	fmt.Println("Nama\t:", John.name)
	fmt.Println("Grade\t:", John.grade)

	// mengisi dengan nama propertynya, tidak harus berurutan
	Joko := student{name: "Joko Anwar", grade: 11}
	fmt.Println("Nama\t:", Joko.name)
	fmt.Println("Grade\t:", Joko.grade)

	var math_nadia = exam{
		subject: "Math",
		score:   90,
		student: student{
			name:  "Nadia",
			grade: 11,
		},
	}

	fmt.Println(math_nadia)

	/* Anonymous struct adalah struct yang tidak dideklarasikan di awal sebagai tipe data baru, melainkan langsung ketika pembuatan object */
	// anonymous struct tanpa pengisian property
	var john = struct {
		person
		grade int
	}{}

	// anonymous struct dengan pengisian property
	var doe = struct {
		person
		grade int
	}{
		person: person{"wick", 21},
		grade:  2,
	}

	Sinta := student2{
		person2: struct {
			name string
			age  int
		}{
			name: "Sinta",
			age:  15,
		},
		grade: 10,
	}

	fmt.Println(john)
	fmt.Println(doe)
	fmt.Println(Sinta)

	/* Method */
	Joko.sayHello()

}

// Penerapan method
func (s student) sayHello() {
	fmt.Println("halo", s.name)
}
