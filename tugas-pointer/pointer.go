package main

import (
	"fmt"
	"math"
)

var luasLingkaran float64
var kelilingLingkaran float64
var jari2 int

// menghitung luas lingkaran dengan parameter jari-jari (menggunakan pointer)
func LuasLingkaran(luas *float64, r *int) {
	jari2 := *r
	*luas = (math.Pi * math.Pow(float64(jari2), 2))
}

// menghitung keliling lingkaran dengan parameter jari-jari (menggunakan pointer)
func KelilingLingkaran(keliling *float64, r *int) {
	*keliling = 2 * math.Pi * (float64(*r))
}

/* funsi introduce menggunakan pointer sebagai parameternya.
 */
func introduce(kalimat *string, nama, gender, profesi, usia string) {
	switch {
	case gender == "laki-laki":
		*kalimat = "Pak " + nama + " adalah seorang " + profesi + " yang berusia " + usia + " tahun."
	case gender == "perempuan":
		*kalimat = "Bu " + nama + " adalah seorang " + profesi + " yang berusia " + usia + " tahun."
	}
}

//fungsi untuk menambah data di slice buah
func tambahBuah(buah *[]string, listBuah ...string) {
	*buah = append(*buah, listBuah...)
}

func main() {
	fmt.Println("======= Soal 1 =======")
	/* buatlah function yang nantinya akan memperbarui value dari luas lingkaran dan keliling lingkaran dengan memasukkan salah satu parameternya yaitu jari-jarinya (wajib menggunakan pointer pada parameter function tersebut) */
	r := &jari2
	jari2 = 7
	LuasLingkaran(&luasLingkaran, r)
	KelilingLingkaran(&kelilingLingkaran, r)

	fmt.Println("Luas Lingkaran = ", luasLingkaran)
	fmt.Println("Keliling Lingkaran = ", kelilingLingkaran)

	fmt.Println("======= Soal 2 =======")
	/* Tulislah sebuah function dengan nama introduce. pastikan semua parameter pada function introduce di gunakan semuanya (wajib menggunakan pointer) */
	var sentence string
	introduce(&sentence, "John", "laki-laki", "penulis", "30")
	fmt.Println(sentence) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"

	introduce(&sentence, "Sarah", "perempuan", "model", "28")
	fmt.Println(sentence) // Menampilkan "Bu Sarah adalah seorang model yang berusia 28 tahun"

	fmt.Println("======= Soal 3 =======")
	/* buatlah function yang menambahkan data di bawah ini ke variabel buah (wajib menggunakan pointer):
	   Jeruk,Semangka,Mangga,Strawberry,Durian,Manggis,Alpukat
	   lalu tampilkan dengan loop dan beri angka di depannya */
	var buah = []string{}
	tambahBuah(&buah, "Jeruk", "Semangka", "Strawberry", "Durian", "Manggis", "Alpukat")
	for no, val := range buah {
		fmt.Println(no+1, ".", val)
	}

	fmt.Println("======= Soal 4 =======")
	/* buatlah function dengan nama tambahDataFilm untuk menambahkan data map[string]string ke slice dataFilm, lalu tampilkan datanya sesuai output yang di inginkan  */
	var dataFilm = []map[string]string{}
	tambahDataFilm("LOTR", "2 jam", "action", "1999", &dataFilm)
	tambahDataFilm("avenger", "2 jam", "action", "2019", &dataFilm)
	tambahDataFilm("spiderman", "2 jam", "action", "2004", &dataFilm)
	tambahDataFilm("juon", "2 jam", "horror", "2004", &dataFilm)

	for no, val := range dataFilm {
		fmt.Print(no+1, ".")
		for key, data := range val {
			fmt.Println("\t", key, ":", data)
		}
	}

}

func tambahDataFilm(judul, durasi, genre, tahun string, dataFilm *[]map[string]string) {
	*dataFilm = append(*dataFilm, map[string]string{
		"title":    judul,
		"duration": durasi,
		"genre":    genre,
		"year":     tahun,
	})
}
