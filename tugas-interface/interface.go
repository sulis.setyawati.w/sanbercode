package main

import (
	"fmt"
	"strconv"
	"strings"
)

type segitigaSamaSisi struct {
	alas, tinggi int
}

type persegiPanjang struct {
	panjang, lebar int
}

type tabung struct {
	jariJari, tinggi float64
}

type balok struct {
	panjang, lebar, tinggi int
}

type hitungBangunDatar interface {
	luas() int
	keliling() int
}

type hitungBangunRuang interface {
	volume() float64
	luasPermukaan() float64
}

type cetak interface {
	cetakData() string
}

type phone struct {
	name, brand string
	year        int
	colors      []string
}

func (sss segitigaSamaSisi) luas() int {
	return sss.alas * sss.tinggi / 2
}

func (sss segitigaSamaSisi) keliling() int {
	return sss.alas * 3
}

func (pp persegiPanjang) luas() int {
	return pp.panjang * pp.lebar
}

func (pp persegiPanjang) keliling() int {
	return 2 * (pp.panjang + pp.lebar)
}

func (b balok) luasPermukaan() float64 {
	return 2 * float64((b.panjang*b.lebar)+(b.lebar*b.tinggi)+(b.tinggi*b.panjang))
}

func (b balok) volume() float64 {
	return float64(b.panjang * b.lebar * b.tinggi)
}

func (p phone) cetakData() string {
	stringdata := "\nname\t: " + p.name + "\nbrand\t:" + p.brand + "\nyear\t:" + strconv.Itoa(p.year) + "\ncolor\t:" + strings.Join(p.colors, ",")
	return stringdata
}

func main() {
	fmt.Println("======= Soal 1 =======")
	/* Soal 1
	gunakanlah interface bangunDatar untuk menentukan luas dan keliling berdasarkan struct segitigaSamaSisi dan persegiPanjang
	lalu gunakanlah interface bangunRuang untuk menentukan volume dan luas permukaan berdasarkan struct tabung dan balok
	print semua hasil perhitungannya */
	var bangunDatar hitungBangunDatar = segitigaSamaSisi{
		alas:   10,
		tinggi: 8,
	}
	fmt.Println("Keliling segitiga = ", bangunDatar.keliling())
	fmt.Println("Luas segitiga = ", bangunDatar.luas())

	bangunDatar = persegiPanjang{
		panjang: 12,
		lebar:   6,
	}
	fmt.Println("Keliling persegi panjang = ", bangunDatar.keliling())
	fmt.Println("Luas persegi panjang = ", bangunDatar.luas())

	var bangunRuang hitungBangunRuang = balok{
		panjang: 10,
		lebar:   8,
		tinggi:  5,
	}
	fmt.Println("Luas permukaan balok = ", bangunRuang.luasPermukaan())
	fmt.Println("Volume balok = ", bangunRuang.volume())

	fmt.Println("======= Soal 2 =======")
	/* buatlah interface yang berisi method dengan tipe data string untuk menampilkan data (nama interface dan nama methodnya bebas)
	lalu tampilkan dengan output yang diinginkan kurang lebih seperti di bawah ini (output ini di simpan dalam method dari interface bukan di print satu-satu) */
	var samsung cetak = phone{
		name:   "Samsung Galxy Note 20",
		brand:  "Samsung",
		year:   2020,
		colors: []string{"Hitam", "Biru"},
	}
	fmt.Println("Data Hp Samsung\n", samsung.cetakData())

	fmt.Println("======= Soal 3 =======")
	/* buatlah function yang tipe data return-nya adalah interface kosong, dengan kondisi:
	jika parameter kedua bernilai true maka tampilkan kalimat (asumsi sisinya 2)"luas persegi dengan sisi 2 cm adalah 4 cm"
	jika parameter kedua bernilai false maka tampilkan hanya hasil angka saja (misal 4)
	jika parameter pertama 0 dan parameter kedua bernilai true tampilkan "Maaf anda belum menginput sisi dari persegi"
	jika parameter pertama 0 dan parameter kedua bernilai false maka tampilkan nil */
	fmt.Println(luasPersegi(4, true))
	fmt.Println(luasPersegi(8, false))
	fmt.Println(luasPersegi(0, true))
	fmt.Println(luasPersegi(0, false))

	fmt.Println("======= Soal 4 =======")
	var prefix interface{} = "hasil penjumlahan dari "
	var kumpulanAngkaPertama interface{} = []int{6, 8}
	var kumpulanAngkaKedua interface{} = []int{12, 14}
	/* gunakan seluruh variabel tersebut untuk menghasilkan output "hasil penjumlahan dari 6 + 8 + 12 + 14 = 40" */
	var arrayAngkaPertama = kumpulanAngkaPertama.([]int)
	var arrayAngkaKedua = kumpulanAngkaKedua.([]int)
	var stringPrefix = prefix.(string)
	var jumlah int
	var angka []string
	for _, val := range arrayAngkaPertama {
		jumlah += val
		angka = append(angka, strconv.Itoa(val))
	}
	for _, val := range arrayAngkaKedua {
		jumlah += val
		angka = append(angka, strconv.Itoa(val))
	}
	result := stringPrefix + strings.Join(angka, " + ") + " = " + strconv.Itoa(jumlah)
	fmt.Println(result)

}

func luasPersegi(sisi int, cond bool) interface{} {
	var result interface{}
	switch {
	case sisi > 0 && cond:
		result = "luas persegi dengan sisi " + strconv.Itoa(sisi) + " cm adalah " + strconv.Itoa(sisi*sisi) + " cm persegi"
	case sisi > 0 && !cond:
		result = sisi * sisi
	case sisi <= 0 && cond:
		result = "Maaf anda belum menginput sisi dari persegi dengan benar"
	case sisi <= 0 && !cond:
		result = nil
	}
	return result
}
